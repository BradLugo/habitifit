// Keep compatibility with older browsers by not using class
var Habitica = function(userId, apiToken) {
    var vars = {
        userId: undefined,
        apiToken: undefined,
        apiUrl: 'https://habitica.com/api/v3'
    };

    var root = this;

    this.construct = function(userId, apiToken) {
        console.log('construct called');
        vars.userId = userId;
        vars.apiToken = apiToken;
    };

    this.newTask = function(newTaskParams) {
        if (newTaskParams && newTaskParams['type' && newTaskParams['text']]) {
            return habiticaClient(vars.apiUrl + '/tasks/user', 'POST', newTaskParams);
        }
    };

    this.getTaskId = function(taskName) {
        root.userTasks().forEach(element => {
            if (element == taskName) {
                return taskName;
            }
        });

        return 'No task found with that name';
    };

    this.taskScoring = function(scoringParams) {
        if (scoringParams && scoringParams['taskId'] && scoringParams['direction']) {
            var endpoint = vars.apiUrl + '/tasks/' + scoringParams['taskId'] + '/score/' + scoringParams['direction'];
            var body = {
                apiToken: vars.apiToken,
                title: scoringParams['title'],
                service: scoringParams['service'],
                icon: scoringParams['icon']
            };
            return habiticaClient(endpoint, 'POST', body);
        }
    };

    this.userStats = function() {
        console.log(vars);
        console.log(habiticaClient(vars.apiUrl + '/user'));
        return habiticaClient(vars.apiUrl + '/user');
    };

    this.userTasks = function(userTasksType) {
        var endpoint = vars.apiUrl + (userTasksType ? '/tasks/user' : '/tasks?type=' + userTasksType);
        return habiticaClient(endpoint);
    };

    this.userGetTask = function(taskId) {
        if (taskId) {
            return habiticaClient(vars.apiUrl + '/tasks/' + taskId);
        }
    };

    this.updateTask = function(updateParams) {
        if (updateParams && updateParams['taskId'] && updateParams['text']) {
            var endpoint = vars.apiUrl + '/tasks/' + updateParams['taskId'];
            return habiticaClient(endpoint, 'PUT', updateParams['text']);
        }
    };

    var habiticaClient = function(endpoint, httpMethod, body) {
        var output;

        $.ajax({
            url: endpoint,
            method: (httpMethod) ? httpMethod : 'GET',
            dataType: 'json',
            async: false,
            headers: {
                'x-api-user': vars.userId,
                'x-api-key': vars.apiToken
            },
            contentType: 'application/json',
            data: body,
            success: function(data) {
                output = data;
            }
        });

        return output;
    }

    this.construct(userId, apiToken);
}
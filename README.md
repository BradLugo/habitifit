# HabitiFit

A one-page Javascript powered application to update Habitica using FitBit data!  It's the perfect match!

##INFO
HabitiFit uses OAuth's implicit grant mechanism. Most data is also stored persistently using HTML5 local storage.

## TO-DO's

- Add push notifications
- Add more FitBit metrics
- Add negative habit support
- Add dailies/todos support
- Move wiki to GitLab